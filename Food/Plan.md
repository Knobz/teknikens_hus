# Plan
## Arduino
### Puzzles
 - Depending on how advanced
 - Buttons take no time
### Communication
 - Wired or wireless
 - - Wireless is cheaper but less reliable
 - - Wired is more reliable but requires more planning

**Puzzles**
 - Small 15-30min/p
 - Big 30-60min/p

**Communication**
 - 30-60min

## Tag reader/writer
### Information
 - Sector 15 seems to be read instantly
 - Stored as hexadecimal
 - Lifetime from last read/write?
 - Tag reader to 0 card
 - Font size 22

**Rfid**
 - 60min

## Raspberry/computer
### Barcode scanner
 - Easy peasy (keyboard interface)
 - Button or timer to reset
 - Ruta som visas när du handlat färdigt
### Graphical interface
 - Web or local
 - - Web is easier for other people
 - - Local is more rigid but also harder for other people to work on
### Database
 - Excel or sqlite
 - - Excel more convenient
 - - Sqlite more professional

**Barcode scanner**
 - 10-15min
**GUI**
 - Web 60-80h
 - Native 25-50h
**Database**
 - Excel 2h
 - Sqlite 5-8h

# Total Estimated Hours
**Worst case**
- 98h

**Best case**
 - 40h