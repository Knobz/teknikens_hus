import sqlite3
import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from barcode_scanner import Barcode_scanner
from kivy.clock import Clock
from kivy.lang import Builder
kivy.require("2.0.0")
from os import getcwd, path

currentPath = path.dirname(__file__)

x = False
if x is True:
    import board
    import time
    import digitalio
    
    led = digitalio.DigitalInOut(board.D18)
    led.direction = digitalio.Direction.OUTPUT

    button = digitalio.DigitalInOut(board.D4)
    button.direction = digitalio.Direction.INPUT
    button.pull = digitalio.Pull.UP

    while True:
        print(button.value)
        led.value = not button.value # light when button is pressed!



#Window.resize = False
Window.size = (1920, 1080)

Window.borderless = True
Window.fullscreen = "auto"
#Window.show_cursor = False
#Window.clear_widgets()
DATABASE = sqlite3.connect("image_database.db")
CURSOR = DATABASE.cursor()

class ui(FloatLayout):
    def __init__(self, **kwargs):
        super(ui, self).__init__(**kwargs)
        
        self.main_layout = FloatLayout()
        self.add_widget(self.main_layout)
        
        self.background_image = Image(source=f"{currentPath}\\Grafik\\Grafik-kassa-bgrd.png", size_hint=(1,1))
        self.main_layout.add_widget(self.background_image)
        
        """self.a = BoxLayout(orientation='vertical', size_hint=(1,1))
        self.main_layout.add_widget(self.a)
        
        self.b = Button(text="hello", size_hint=(1, 0.10))
        self.a.add_widget(self.b)
        
        self.q = BoxLayout(orientation='horizontal',size_hint=(1, 0.9))
        self.a.add_widget(self.q)
        
        self.r = BoxLayout(orientation='horizontal', size_hint=(.4, 1))
        self.q.add_widget(self.r)"""
        
        
        #layout = GridLayout(cols = 2, row_force_default = True,
        #                    row_default_height = 30)
        
        """self.pp = GridLayout(size_hint=(.5, 1), rows=10, cols=1,row_default_height = 30)
        self.r.add_widget(self.pp)
        self.qq = GridLayout(size_hint=(.5, 1), rows=10, cols=1, row_default_height = 30)
        self.r.add_widget(self.qq)"""
        
        
        
        self.main_boxlayout = BoxLayout(orientation="horizontal", pos=(180,140), size_hint=(None, None), size=(400,500))
        self.main_layout.add_widget(self.main_boxlayout)
        
        self.goods = BoxLayout(orientation="vertical",size_hint=(0.5,1))
        self.price = BoxLayout(orientation="vertical",size_hint=(0.5,1))
        
        self.main_boxlayout.add_widget(self.goods)
        self.main_boxlayout.add_widget(self.price)
        
        self.main_layout.add_widget(Image(source=f"{currentPath}\\Grafik\\Knappar - släckta.png", size_hint=(None, None), size=(400,183), pos_hint={"x":0, "top":1}))
        
        """ self.s0 = Label(color=(0,0,0,1))
        self.s1 = Label(color=(0,0,0,1))
        self.s2 = Label(color=(0,0,0,1))
        self.s3 = Label(color=(0,0,0,1))
        self.s4 = Label(color=(0,0,0,1))
        self.s5 = Label(color=(0,0,0,1))
        self.s6 = Label(color=(0,0,0,1))
        self.s7 = Label(color=(0,0,0,1))
        self.s8 = Label(color=(0,0,0,1))
        self.s9 = Label(color=(0,0,0,1))
        """
        
        self.s0 = Button(text="L")
        self.s1 = Button(text="L")
        self.s2 = Button(text="L")
        self.s3 = Button(text="L")
        self.s4 = Button(text="L")
        self.s5 = Button(text="L")
        self.s6 = Button(text="L")
        self.s7 = Button(text="L")
        self.s8 = Button(text="L")
        self.s9 = Button(text="L")
        
        self.goods.add_widget(self.s0)
        self.goods.add_widget(self.s1)
        self.goods.add_widget(self.s2)
        self.goods.add_widget(self.s3)
        self.goods.add_widget(self.s4)
        self.goods.add_widget(self.s5)
        self.goods.add_widget(self.s6)
        self.goods.add_widget(self.s7)
        self.goods.add_widget(self.s8)
        self.goods.add_widget(self.s9)
        
        
        self.t0 = Button(text="R")
        self.t1 = Button(text="R")
        self.t2 = Button(text="R")
        self.t3 = Button(text="R")
        self.t4 = Button(text="R")
        self.t5 = Button(text="R")
        self.t6 = Button(text="R")
        self.t7 = Button(text="R")
        self.t8 = Button(text="R")
        self.t9 = Button(text="R")
        
        self.price.add_widget(self.t0)
        self.price.add_widget(self.t1)
        self.price.add_widget(self.t2)
        self.price.add_widget(self.t3)
        self.price.add_widget(self.t4)
        self.price.add_widget(self.t5)
        self.price.add_widget(self.t6)
        self.price.add_widget(self.t7)
        self.price.add_widget(self.t8)
        self.price.add_widget(self.t9)
        
        self.s_list = [self.s0,self.s1,self.s2,self.s3,self.s4,self.s5,self.s6,self.s7,self.s8,self.s9]
        #self.t_list = [self.t0,self.t1,self.t2,self.t3,self.t4,self.t5,self.t6,self.t7,self.t8,self.t9]
        
        """self.d = Button(text="moonman", size_hint=(0.5, 1))
        self.q.add_widget(self.d)
        
        self.e = Button(text="hello", size_hint=(0.45, 0.15))
        self.a.add_widget(self.e)"""
        
        """
        self.b = Label(text="hello", font_size=30,size_hint=(1, .15), pos_hint={'top': 1})
        self.add_widget(self.b)
        self.d = Label(text="fsdfmdsjkldfjks", font_size=30,size_hint=(.5, 1),pos_hint={"right": 1})
        self.add_widget(self.d)
        self.price_and_wallet = FloatLayout(size_hint=(.5,.1), pos_hint={"down": 1})
        self.available = Label(text=f"{200}kr", font_size=30,size_hint=(.25, 1), pos_hint={"left": 1})
        self.price_and_wallet.add_widget(self.available)
        self.price = Label(text=f"{50}kr", font_size=30,size_hint=(.25, 1),pos_hint={"right": 1})
        self.price_and_wallet.add_widget(self.price)
        self.add_widget(self.price_and_wallet)
        """
    
        
        
class hello(App):
    def build(self):
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.mainloop, 1 / 30.)
        self.new_gui = ui()
        return self.new_gui
        
    def mainloop(self, dt):
        if self.barcode.result:
            print(self.barcode.result)
            CURSOR.execute("SELECT image_name FROM image_paths WHERE barcode = '%s'" % self.barcode.result)
            match = CURSOR.fetchone()
            
            name = ""
            if match:
                name = match[0][:-4]
                print(name)
                
            if name:
                for i in range(10):
                    label = self.new_gui.s_list[i]
                    if label.text == name:
                        #self.new_gui.t_list[i].text = str(int(self.new_gui.t_list[i].text) + 10)
                        break
                else:
                    for i in range(10):
                        label = self.new_gui.s_list[i]
                        if label.text == "":
                            label.text = name
                            #self.new_gui.t_list[i].text = "10"
                            break    
                
                    
                
                
            """
            if self.new_gui.ss.text == "":
                self.new_gui.ss.text = name[:-4]
                self.new_gui.h.text = "10"
            elif self.new_gui.ss.text == name[:-4]:
                self.new_gui.h.text = str(int(self.new_gui.h.text) + 10)
            elif self.new_gui.ss.text != "" and self.new_gui.ss.text != name[:-4]:
                if self.new_gui.sss.text == name[:-4]:
                    self.new_gui.j.text = str(int(self.new_gui.j.text) + 10)
                else:
                    self.new_gui.sss.text = name[:-4]
                    self.new_gui.j.text = "10"
            """
            
            self.barcode.result = ""
        

hello().run()
