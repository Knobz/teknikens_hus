#! /usr/bin/env python
import os
os.environ["KIVY_GL_BACKEND"] = "gl"
import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.uix.image import Image
from barcode_scanner import Barcode_scanner
import sqlite3
from os import getcwd, system
#from playsound import playsound
import csv

PATH = "/home/pi/Documents/teknikens_hus/"
# csv file name


# initializing the titles and rows list



DEFAULT_IMAGE = "gomspene.png"

Window.borderless = True
Window.fullscreen = "auto"

Window.show_cursor = False


class GUI(App):
    def build(self):
        self.img = Image(source=PATH + "Food/ui/Default/" + DEFAULT_IMAGE, nocache=True)
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.check_barcode,0.1)
        self.current_information_timer = None
        return self.img

    def change_image(self, source):
        self.img.source = source

    def information_timer_start(self):
        self.current_information_timer = Clock.schedule_once(self.default_image, 12)

    def default_image(self, dt):
        self.change_image(PATH + "Food/ui/Default/gomspene.png")

    def check_barcode(self, dt):
        if self.barcode.scanned_code:
            source = self.image_source(self.barcode.result)
            print(self.image_source(self.barcode.result))
            print(f"This is the code: {self.barcode.result}")
            self.change_image(source)
            self.barcode.scanned_code = []
            self.barcode.result = ""

    def image_source(self, barcode):
        if self.current_information_timer is not None:
            self.current_information_timer.cancel()
        if "numpad" in barcode:
            barcode = barcode[-1]
        match = ""
        fields = []
        rows = []

# reading csv file
        filename = PATH + "Food/ui/Munnen.csv"
        with open(filename, 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';')

    # extracting field names through first row
            fields = next(csvreader)

    # extracting each data row one by one
            for row in csvreader:
                rows.append(row)
        for row in rows:
            if str(row[1]).replace(" ", "") == str(barcode):
                match = str(row[0]) + ".jpg"
                break
        
        if match:
            source =  PATH + "Food/ui/Grafik_munnen/" + match #Fetch one element
        else:
            source = PATH + "Food/ui/Ingen_vara/ingen_vara.png"
        self.information_timer_start()
        return source
    

    
GUI().run()
