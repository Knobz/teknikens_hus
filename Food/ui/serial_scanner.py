import serial
import time

linux = "/dev/ttyUSB0"
class arduino:
    def __init__(self) -> None:
        try:
            self.serial_port = serial.Serial("COM19", 9600)
        except:
            print("Failed to connect to arduino")
    
    def read_serial(self):
        message = self.serial_port.readline().decode("utf-8")
        return message

    def write_serial(self, message):
        respons = self.serial_port.write(bytes(message, "utf-8"))
        return respons
    
    def close(self):
        self.serial_port.close()

if __name__ == "__main__":
    ard = arduino()
    message = int(ard.read_serial())
    time.sleep(2)
    print(message)
    
