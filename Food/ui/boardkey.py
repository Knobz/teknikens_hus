import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.vkeyboard import VKeyboard
from kivy.core.window import Window
kivy.require("2.0.0")

Window.show_cursor = True
Window.size = (700,300)

class MyApp(App):
    def build(self):
        self.label_text = ""
        self.capitalized = False
        layout = GridLayout(cols=1)
        keyboard = VKeyboard(on_key_up = self.key_up)
        self.label = Label(text = f"X")
        
        layout.add_widget(self.label)
        layout.add_widget(keyboard)
        
        return layout
    
    def key_up(self, obj, keycode, *args):
        
        if str(keycode) == "backspace":
            if len(self.label_text) > 0:
                self.label_text = self.label_text[:-1]
            else:
                pass
            self.label.text = self.label_text 
        elif str(keycode) == "enter":
            self.label.text = "Place your card on the reader"
        else:
            if str(keycode) == "capslock":
                self.capitalized = not self.capitalized
                return
            if self.capitalized == True:
                self.label_text = self.label_text  + str(keycode).upper()
                self.label.text = self.label_text
            else:
                self.label_text = self.label_text  + str(keycode)
                self.label.text = self.label_text
        
        
        


if __name__ == '__main__':
    MyApp().run()