import board
import busio
import digitalio
import time

bt = digitalio.DigitalInOut(board.D27)
bt.direction = digitalio.Direction.INPUT
#bt.pull = digitalio.Pull.UP

led = digitalio.DigitalInOut(board.D14)
led.direction = digitalio.Direction.OUTPUT


led.value = True

while True:
	print(bt.value)
	led.value = not bt.value
	time.sleep(1)
