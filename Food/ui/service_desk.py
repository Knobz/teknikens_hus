import kivy
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.core.window import Window
kivy.require("2.0.0")
import serial
import serial.tools.list_ports

#Window.show_cursor = True
#Window.size = (700,300)

class TeknikensHusApp(App):
    def build(self):
        self.icon = "logga-tek.png"
        self.layout = BoxLayout(orientation="vertical")
        self.lbl = Label(text="""Skriv ett namn i rutan nedanför
När du är färdig trycker du på enter eller "skriv".""", font_size=30, halign="center")
        self.inp = TextInput(text="", multiline=False, font_size=30)
        self.button_layout = BoxLayout(orientation="horizontal")
        self.layout.add_widget(self.lbl)       
        self.layout.add_widget(self.inp)
        self.layout.add_widget(self.button_layout)
        self.write = Button(text="Skriv", font_size=30, background_color=(0,1,0,1), on_press=self.on_enter)
        self.button_layout.add_widget(self.write)
        
        self.inp.focus = True
        self.inp.bind(on_text_validate=self.on_enter)
        Clock.schedule_interval(self.text_length, 1 / 30.)
        return self.layout
        
    def text_length(self, dt):
        if len(self.inp.text) > 16:
            self.inp.foreground_color = (1,0,0,1)
            self.write.disabled = True
        if len(self.inp.text) <= 16:
            self.inp.foreground_color = (0,0,0,1)
            self.write.disabled = False
            
    def on_enter(self, event):
        ports = list(serial.tools.list_ports.comports())
        for p in ports:
            print(p)
            if "Arduino" in p.description:
                print(p)
        
        with serial.Serial("COM19", 9600) as s:
            s.write(bytes(self.inp.text, encoding='utf8'))
        print("writing to card")
    
    
    


        
        


if __name__ == '__main__':
    TeknikensHusApp().run()