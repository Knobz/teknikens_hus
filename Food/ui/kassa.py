from sqlite3.dbapi2 import version
import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from barcode_scanner import Barcode_scanner
from kivy.clock import Clock
from kivy.lang import Builder
kivy.require("2.0.0")
from os import getcwd

x = False
if x is True:
    import board
    import time
    import digitalio
    
    led = digitalio.DigitalInOut(board.D18)
    led.direction = digitalio.Direction.OUTPUT

    button = digitalio.DigitalInOut(board.D4)
    button.direction = digitalio.Direction.INPUT
    button.pull = digitalio.Pull.UP

    while True:
        print(button.value)
        led.value = not button.value # light when button is pressed!



Window.resize = False
#Window.size = (1920, 1080)

Window.borderless = True
Window.fullscreen = "auto"
#Window.show_cursor = False
#Window.clear_widgets()

class ui(FloatLayout):
    def __init__(self, **kwargs):
        super(ui, self).__init__(**kwargs)
        
        #self.barcode = ""
        print(self.barcode)
        
        self.b = Label(text="hello", font_size=30,size_hint=(1, .15), pos_hint={'top': 1})
        self.add_widget(self.b)
        #self.c = Label(text="hello thos", font_size=30,size_hint=(.5, .75), pos_hint={"left": 1})
        #self.add_widget(self.c)
        self.produce = BoxLayout(size_hint=(.5, .75), pos_hint={"left": 1})
        self.produce_name = BoxLayout(pos_hint={"left": 1})
        self.produce_name = Label(text=f"{200}kr" ,font_size=30,size_hint=(.25, 1), pos_hint={"left": 1})
        self.produce.add_widget(self.produce_name)
        self.produce_price = Label(text=f"{50}kr", font_size=30,size_hint=(.25, 1),pos_hint={"right": 1})
        self.produce.add_widget(self.produce_price)
        self.add_widget(self.produce)
        
        
        self.d = Label(text="fsdfmdsjkldfjks", font_size=30,size_hint=(.5, 1),pos_hint={"right": 1})
        self.add_widget(self.d)
        
        
        
        self.price_and_wallet = FloatLayout(size_hint=(.5,.1), pos_hint={"down": 1})
        self.available = Label(text=f"{200}kr", font_size=30,size_hint=(.25, 1), pos_hint={"left": 1})
        self.price_and_wallet.add_widget(self.available)
        self.price = Label(text=f"{50}kr", font_size=30,size_hint=(.25, 1),pos_hint={"right": 1})
        self.price_and_wallet.add_widget(self.price)
        self.add_widget(self.price_and_wallet)
        
        self.add_produce_to_cart("x")
        
    def add_produce_to_cart(self, image):
        self.product = Label(text=f"sallad", font_size=30,size_hint=(.5, 0.1))
        self.produce_name.add_widget(self.product)
        
        self.product1 = Label(text=f"sallad", font_size=30,size_hint=(.5, 0.1))
        self.produce_name.add_widget(self.product1)
        
        new = Image(source=getcwd() + "\\ui\\products\\sallad.png")
        new.width = 192
        new.height = 108
        new.pos = (1000,400)
        self.d.add_widget(new)
        pass
        
        
class hello(App):
    def build(self):
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.mainloop, 1 / 30.)
        new_gui = ui
        new_gui.barcode = "hej"
        return new_gui()
        
    def mainloop(self, dt):
        if self.barcode.result:
            print(self.barcode.result)
            
            self.barcode.result = ""
        

hello().run()
