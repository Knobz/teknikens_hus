from tkinter import *
from PIL import ImageTk, Image

PATH = "C:\\Users\\admin\\Documents\\Python\\Teknikens hus\\teknikens_hus\\Food\\ui\\"
im1 = PATH + "Grafik\\ägg.png"
im2 = PATH + "Grafik\\bröd.png"
im3 = PATH + "Grafik\\mjölk.png"

root= Tk()
plant_stat_panel = Label(root)
plant_stat_panel.grid(row = 5,column = 0, columnspan=2, sticky = W )
x = 0
def img_updater():
    global x
    x = x + 1
    
    if x == 0:
        img = im1
    elif x == 1:
        img = im2
    else:
        img = im3
        
    plant_stat_img = ImageTk.PhotoImage(Image.open(img))#/home/pi/html/
    plant_stat_panel.config(image = plant_stat_img)
    plant_stat_panel.image = plant_stat_img
    root.after(5000, img_updater)

img_updater()
root.mainloop() 