#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

#define button 2
#define led 3 
#define buzzer 9

int clean_card = 1;
unsigned long prev = millis();

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
}

void loop() {
  byte clean_buffer[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte name_buffer[17] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte len;
  // put your main code here, to run repeatedly:
  clean_card = digitalRead(button);
  digitalWrite(led, HIGH);
  if (Serial.available() > 0) {
    len = Serial.readBytesUntil('#', (char *) name_buffer, 15) ; // read family name from serial
    for (byte i = len; i < 15; i++) name_buffer[i] = 0;
    clean_card = 0;
  }
  
  
  if (clean_card == 0){
    for (byte i = 0; i < 16; i++) {Serial.print(char(name_buffer[i]));}
    clean(clean_buffer, name_buffer);
    playTone();
    led_blink();
  }
  
}

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100);
  }
  
void led_blink(){
  digitalWrite(led, LOW);
  delay(200);
  digitalWrite(led, HIGH);
  delay(200);
  digitalWrite(led, LOW);
  delay(200);
  digitalWrite(led, HIGH);
  delay(200);
  digitalWrite(led, LOW);
  delay(200);
  digitalWrite(led, HIGH);
  delay(200);
  digitalWrite(led, LOW);
  delay(200);
  }

void waitingToWrite(){
  if (millis() - prev > 250){digitalWrite(led, HIGH);}
  if (millis() - prev > 500){digitalWrite(led, LOW); prev = millis();}
  }

void clean(byte clean_buffer[16], byte name_buffer[16]){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock = 11;
  byte valueBlockA = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  while (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){waitingToWrite();}
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, clean_buffer, buffersize_write);
  delay(500);
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA + 1, name_buffer, buffersize_write);
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  Serial.println("Done");
}
