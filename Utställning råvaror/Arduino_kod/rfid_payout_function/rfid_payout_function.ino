#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

int puzzle_value = 135;

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  Serial.begin(9600);
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
}

void loop() {
  payout();
  delay(1000);

}

void payout(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  
  Serial.println("Ready to start");
  while ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    Serial.print(buffer_read[i]);
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  Serial.println();
  Serial.print("Total on card : ");
  Serial.println(total);
  total = total + puzzle_value;
  int temp = total;
  Serial.print("Total after puzzle added: ");
  Serial.println(total);
  
  while (temp >= 10){total_count++; temp = temp/10;}
  
  for (byte i = 0; i < total_count + 1; i++){
    if (i == 0){cash[total_count + 1] = 99;}
    cash[total_count - i] = total%10;
    total = total/10;  
    
  }
  Serial.println();
  for (byte i = 0; i < 16; i++){Serial.print(cash[i]);}
  Serial.println();


  //Write to the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  delay(500);
  //**************************************
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  Serial.println("Done Reading");
}
