#define relay1 5
#define relay2 6
#define button 7
unsigned long prev;
int buttonState;
int x = 0;
int waitTime = 30;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(relay1, OUTPUT);
  pinMode(relay2, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  
}

// the loop function runs over and over again forever
void loop() {
  if (x == 1){
    prev = millis();
    x = 0;  
  }
   buttonState = digitalRead(button);
  if (buttonState == LOW && millis() - prev > waitTime * 1000){
    digitalWrite(relay1, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(500);                       // wait for a second
    digitalWrite(relay2, HIGH);
    delay(1000); 
    digitalWrite(relay1, LOW);    // turn the LED off by making the voltage LOW
    digitalWrite(relay2, LOW);
    delay(100);
    x = 1;
  }                      // wait for a second
}
