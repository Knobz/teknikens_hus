#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  Serial.begin(9600);  // Initialize serial communications with the PC for debugging.
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
  Serial.println("Ready to scan:");
}

void loop(){
    MFRC522::MIFARE_Key key;
    for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
    byte trailerBlock   = 7;
    byte valueBlockA    = 4;
    byte buffer[18];
    byte buffersize = sizeof(buffer);
    
    while ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()) {}
    byte status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
    status = mfrc522.MIFARE_Read(valueBlockA, x, &buffersize);
    for (byte i = 0; i < 16; i++){Serial.print(char(x[i]));}
    for (byte i = 0; i < 16; i++){Serial.print(x[i]);}
    Serial.println("Done reading");
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
}