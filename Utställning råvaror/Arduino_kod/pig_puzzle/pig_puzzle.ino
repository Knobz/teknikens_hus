#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

#define button_pin 2
#define led_pin 3

#define first_pig 4
#define second_pig 5
#define first_pig_led 7
#define second_pig_led 8

#define buzzer 9

int fade_value = 0;
int fade_direction = 1;
int blink_direction = 1;
int start_value = 0;
int payout_value = 0;
int button_value;

int first_pig_done = 0;
int second_pig_done = 0;
unsigned long prev = millis();
unsigned long win_timer = 0;
const int puzzle_value = 110;

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  pinMode(led_pin, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(button_pin, INPUT_PULLUP);
  pinMode(first_pig, INPUT_PULLUP);
  pinMode(second_pig, INPUT_PULLUP);
  pinMode(first_pig_led, OUTPUT);
  pinMode(second_pig_led, OUTPUT);
  Serial.begin(9600);
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
  Serial.println(F("Ready for action"));
}

void loop() {
  waitingToStart();
  button_value = digitalRead(button_pin);
  if(button_value == 1){}
  else{start_puzzle();}
  

}

void start_puzzle(){
  digitalWrite(led_pin, HIGH);
  delay(200);
  first_pig_done = 0;
  second_pig_done = 0;
  while(true){
    int fp = digitalRead(first_pig);
    int sp = digitalRead(second_pig);
    int res = digitalRead(button_pin);
    if (res == 0){
      digitalWrite(first_pig_led, LOW);
      digitalWrite(second_pig_led, LOW);
      first_pig_done = 0;
      second_pig_done = 0;
      return;
      }
    if (fp == 0){first_pig_done=1; digitalWrite(first_pig_led, HIGH);}
    if (sp == 0){second_pig_done=1; digitalWrite(second_pig_led, HIGH);}

    if (first_pig_done == 1 && second_pig_done == 1){
      win_timer = millis();
      win();
      payout();
      digitalWrite(first_pig_led, LOW);
      digitalWrite(second_pig_led, LOW);
      first_pig_done = 0;
      second_pig_done = 0;
      return;
      }
    }
  }



void waitingToPayout(){
  if (millis() - prev > 250){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 500){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void waitingToStart(){
  if (millis() - prev > 3000){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 4000){digitalWrite(led_pin, LOW); prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100);
  }
void win(){
        int NOTE_SUSTAIN = 30;
         for(uint8_t nLoop = 0;nLoop < 2;nLoop ++)
         {
           tone(buzzer,1000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1100);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1200);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1300);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1400);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1500);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1600);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1700);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1800);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1900);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2100);
           delay(NOTE_SUSTAIN);
         }
         noTone(buzzer);
  }
  

void payout(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  
  Serial.println("Ready to start");
  while ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){waitingToPayout(); if (digitalRead(button_pin) == 0){return;} if (millis() - win_timer > 60000){return;}}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    Serial.print(buffer_read[i]);
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  Serial.println();
  Serial.print("Total on card : ");
  Serial.println(total);
  total = total + puzzle_value;
  int temp = total;
  Serial.print("Total after puzzle added: ");
  Serial.println(total);
  
  while (temp >= 10){total_count++; temp = temp/10;}
  
  for (byte i = 0; i < total_count + 1; i++){
    if (i == 0){cash[total_count + 1] = 99;}
    cash[total_count - i] = total%10;
    total = total/10;  
    
  }
  Serial.println();
  for (byte i = 0; i < 16; i++){Serial.print(cash[i]);}
  Serial.println();


  //Write to the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  delay(500);
  //**************************************
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  playTone();
  Serial.println("Done Reading");
}
