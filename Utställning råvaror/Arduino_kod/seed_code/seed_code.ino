#define button 2
#define led_pin 3
#define green 5
#define red 4

#define heat 6
#define CO2 7
#define poop 8
#define water 10
#define sun 11
#define finish 12

#define oil A0
#define cold A1
#define candy A2

#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

const int buzzer = 9;
const int puzzle_value = 55;


MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

int done1 = 0;
int done2 = 0;
int done3 = 0;
int done4 = 0;
int done5 = 0;
int started = 0;

unsigned long prev = millis();
unsigned long win_timer = 0;
int count = 0;
int ready_to_play = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(green, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(led_pin, OUTPUT);
  
  pinMode(button, INPUT_PULLUP);
  pinMode(heat, INPUT_PULLUP);
  pinMode(CO2, INPUT_PULLUP);
  pinMode(poop, INPUT_PULLUP);
  pinMode(water, INPUT_PULLUP);
  pinMode(sun, INPUT_PULLUP);
  pinMode(finish, INPUT_PULLUP);
  pinMode(candy, INPUT_PULLUP);
  pinMode(oil, INPUT_PULLUP);
  pinMode(cold, INPUT_PULLUP);
  Serial.begin(9600);
  pinMode(buzzer, OUTPUT);
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  int start_value = digitalRead(button);
  if (start_value == 0){ready_to_play = 1;}
  delay(250);
  while(ready_to_play == 1){
    int heat_value = digitalRead(heat);
    int CO2_value = digitalRead(CO2);
    int poop_value = digitalRead(poop);
    int water_value = digitalRead(water);
    int sun_value = digitalRead(sun);
    int finish_value = digitalRead(finish);
    start_value = digitalRead(button);
    
    int candy_value = analogRead(candy);
    int oil_value = analogRead(oil);
    int cold_value = analogRead(cold);
  
    digitalWrite(led_pin, HIGH);
    
    if (heat_value == 0){digitalWrite(green, HIGH); if(done1 == 0){count = count + 1; partGoal(count);} done1 = 1; return;}
    if (CO2_value == 0){digitalWrite(green, HIGH); if(done2 == 0){count = count + 1; partGoal(count);} done2 = 1; return;}
    if (poop_value == 0){digitalWrite(green, HIGH); if(done3 == 0){count = count + 1; partGoal(count);} done3 = 1; return;}
    if (water_value == 0){digitalWrite(green, HIGH); if(done4 == 0){count = count + 1; partGoal(count);} done4 = 1; return;}
    if (sun_value == 0){digitalWrite(green, HIGH); if(done5 == 0){count = count + 1; partGoal(count);} done5 = 1; return;}
    if (oil_value < 800 || cold_value < 800 || candy_value < 800){
      digitalWrite(red, HIGH); digitalWrite(green, LOW); fail(); reset(); delay(1000); digitalWrite(red, LOW);
      }
    if (done1 == 1 && done2 == 1 && done3 == 1 && done4 == 1 && done5 == 1){
    if (finish_value == 0){Serial.print("Finished"); win_timer = millis(); win(); payout(); reset();}
      /* Payout function */}
    if(start_value == 0){break;}
    }
    reset();
}



void reset(){
  done1 = 0;
  done2 = 0;
  done3 = 0;
  done4 = 0;
  done5 = 0;
  started = 0;
  count = 0;
  digitalWrite(green, LOW);
  ready_to_play = 0;
  
  }

void waitingToPayout(){
  if (millis() - prev > 250){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 500){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void waitingToStart(){
  if (millis() - prev > 3000){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 4000){digitalWrite(led_pin, LOW); prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100); 
  }

void partGoal(int count){
    for (int i = 0; i < count; i++){
      tone(buzzer, 1000 + 100*i);
      delay(100);
      noTone(buzzer);
      delay(100);
      }
     noTone(buzzer);
  }
void fail(){
  tone(buzzer, 700);
  delay(200);
  noTone(buzzer);
  delay(100);
  tone(buzzer, 700);
  delay(200);
  noTone(buzzer);
  }

void win(){
        int NOTE_SUSTAIN = 30;
         for(uint8_t nLoop = 0;nLoop < 2;nLoop ++)
         {
           tone(buzzer,1000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1100);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1200);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1300);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1400);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1500);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1600);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1700);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1800);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1900);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2100);
           delay(NOTE_SUSTAIN);
         }
         noTone(buzzer);
  }
  
void payout(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  
  Serial.println("Ready to start");
  while ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){waitingToPayout(); if (digitalRead(button) == 0){return;} if (millis() - win_timer > 60000){return;}}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    Serial.print(buffer_read[i]);
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  total = total + 1;
  Serial.println();
  Serial.print("Total on card : ");
  Serial.println(total);
  total = total + puzzle_value;
  int temp = total;
  Serial.print("Total after puzzle added: ");
  Serial.println(total);
  
  while (temp >= 10){total_count++; temp = temp/10;}
  
  for (byte i = 0; i < total_count + 1; i++){
    if (i == 0){cash[total_count + 1] = 99;}
    cash[total_count - i] = total%10;
    total = total/10;  
    
  }
  Serial.println();
  for (byte i = 0; i < 16; i++){Serial.print(cash[i]);}
  Serial.println();


  //Write to the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  delay(500);
  //**************************************
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  playTone();
  Serial.println("Done Reading");
}
