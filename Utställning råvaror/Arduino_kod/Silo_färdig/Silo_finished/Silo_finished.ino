#include <Adafruit_NeoPixel.h>

//******************************
#define skimmed_milk_button 4
#define cream_button 5
#define homogenizer_button 31
#define bacteria_button 7
#define butter_button 22
#define reset_button 2

#define pasturizing_cold_neopixel 10 //20 leds
#define pasturizing_warm_neopixel 6 //20 leds
#define homogenizer_neopixel 12 //4 leds
#define butter_neopixel 8 //4 leds

#define homogenizer_led 13
#define four_way_led1 34
#define four_way_led2 35
#define four_way_led3 36
#define four_way_led4 37
#define four_way_button1 24
#define four_way_button2 26
#define four_way_button3 28
#define led_pin 3
//*******************************

Adafruit_NeoPixel pixels_cold(20, pasturizing_cold_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_warm(20, pasturizing_warm_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_homogenizer(4, homogenizer_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_butter(4, butter_neopixel, NEO_GRB + NEO_KHZ800);

#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

const int buzzer = 9;
const int milk_value = 25;
const int cheese_value = 75;
const int butter_milk_value = 55;
const int butter_value = 60;

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

int puzzle_value = 0;
int butter_finished = 0;
int butter_milk_finished = 0;
int milk_finished = 0;
int cheese_finished = 0;
int butter_credits_added = 0;
int butter_milk_credits_added = 0;
int milk_credits_added = 0;
int cheese_credits_added = 0;
unsigned long prev = millis();
unsigned long hom_prev = millis();
int started = 0;
int ready_for_payout = 0;
int past = 0;
int butter_past = 0;
int butt = 0;
int hom = 0;
int mil = 0;
int completed_puzzles = 0;
int past_color_fade = 1;
int butter_past_color_fade = 1;
int butter_color_fade = 1;
int hom_color_fade = 1;
int mil_color_fade = 1;
int past_no_fade = 0;
int butter_past_no_fade = 0;
int butt_no_fade = 0;
int hom_no_fade = 0;
int mil_no_fade = 0;
int x = 0;
int y = 0;
int z = 0;
int res = 0;
int butter_milk_partgoal = 0;
int milk_partgoal = 0;

void setup() {
  Serial.begin(9600);
  Serial.print("begin");
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
  // put your setup code here, to run once:
  
  pinMode(skimmed_milk_button, INPUT_PULLUP);
  pinMode(cream_button, INPUT_PULLUP);
  pinMode(homogenizer_button, INPUT_PULLUP);
  pinMode(bacteria_button, INPUT_PULLUP);
  pinMode(butter_button, INPUT_PULLUP);
  pinMode(reset_button, INPUT_PULLUP);

  pinMode(four_way_button1, INPUT_PULLUP);
  pinMode(four_way_button2, INPUT_PULLUP);
  pinMode(four_way_button3, INPUT_PULLUP);

  pinMode(four_way_led1, OUTPUT);
  pinMode(four_way_led2, OUTPUT);
  pinMode(four_way_led3, OUTPUT);
  pinMode(four_way_led4, OUTPUT);
  pinMode(led_pin, OUTPUT);
  pinMode(homogenizer_led, OUTPUT);

  pixels_cold.begin();
  pixels_warm.begin();
  pixels_homogenizer.begin();
  pixels_butter.begin();

  pixels_cold.show();
  pixels_warm.show();
  pixels_homogenizer.show();
  pixels_butter.show();
  
  pixels_cold.setBrightness(50);
  pixels_warm.setBrightness(50);
  pixels_homogenizer.setBrightness(100);
  pixels_butter.setBrightness(100);
  reset();
  Serial.println("Line 1 : 2 : 3 ");
}



void loop() {
  
  x = digitalRead(four_way_button1);
  y = digitalRead(four_way_button2);
  z = digitalRead(four_way_button3);
  res = digitalRead(reset_button);
  
  if (res == 0){started = 1; delay(500);}
  if (started == 0){waitingToStart(); return;}
  if (ready_for_payout == 1){payout();}
  res = digitalRead(reset_button);
  
  if (x == 1 && y == 0){
    if( butter_finished == 0){
    butter();
    }
    else{
      pixels_butter.fill(pixels_butter.Color(255,125,0));
      pixels_warm.fill(pixels_butter.Color(255,0,0));
      pixels_cold.fill(pixels_butter.Color(0,0,255));
      pixels_warm.show();
      pixels_cold.show();
      pixels_butter.show();
    }
    }
  else if (x == 1 && y == 1){
    if( milk_finished == 0){
    milk();
    }
    else{
      pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255));
      pixels_warm.fill(pixels_butter.Color(255,0,0));
      pixels_cold.fill(pixels_butter.Color(0,0,255));
      pixels_warm.show();
      pixels_cold.show();
      pixels_homogenizer.show();
    }
    }
  else if (x == 0 && z == 0){
    if( butter_milk_finished == 0){
    butter_milk();
    }
    else{
      pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255));
      pixels_warm.fill(pixels_butter.Color(255,0,0));
      pixels_cold.fill(pixels_butter.Color(0,0,255));
      pixels_warm.show();
      pixels_cold.show();
      pixels_homogenizer.show();
    }
    }
  else if (x == 0 && z == 1){
    if( cheese_finished == 0){
    cheese();
    }
    else{
      pixels_warm.fill(pixels_butter.Color(255,0,0));
      pixels_cold.fill(pixels_butter.Color(0,0,255));
      pixels_warm.show();
      pixels_cold.show();
    }
    }
  
  if (butter_milk_finished == 1){digitalWrite(four_way_led2, HIGH); if (butter_milk_credits_added == 0){digitalWrite(homogenizer_led, LOW);reset_leds();puzzle_value = puzzle_value + butter_milk_value; win();} butter_milk_credits_added = 1;}
  if (milk_finished == 1){digitalWrite(four_way_led1, HIGH); if (milk_credits_added == 0){digitalWrite(homogenizer_led, LOW);reset_leds();puzzle_value = puzzle_value + milk_value; win();} milk_credits_added = 1;}
  if (butter_finished == 1){digitalWrite(four_way_led4, HIGH); if (butter_credits_added == 0){digitalWrite(homogenizer_led, LOW);reset_leds();puzzle_value = puzzle_value + butter_value; win();} butter_credits_added = 1;}
  if (cheese_finished == 1){digitalWrite(four_way_led3, HIGH); if (cheese_credits_added == 0){digitalWrite(homogenizer_led, LOW);reset_leds();puzzle_value = puzzle_value + cheese_value; win();} cheese_credits_added = 1;}
  if (butter_milk_finished == 1 || butter_finished == 1 || milk_finished == 1 || cheese_finished == 1){ready_for_payout = 1; payout();}
  else{digitalWrite(led_pin, HIGH);}
  if (res == 0){reset();}
  
  
}


void butter(){
  past = 0;
  past_no_fade = 0;
  past_color_fade = 1;
  digitalWrite(cream_button, HIGH);
  pinMode(cream_button, INPUT_PULLUP);
  int pasturization_value = digitalRead(cream_button);
  int butter_value = digitalRead(butter_button);
  
  if (butter_past == 0){
    if (butter_past_no_fade == 0){
      for(int i = 0; i < 256; i++){
        digitalWrite(led_pin, HIGH);
        res = digitalRead(reset_button);
        pasturization_value = digitalRead(cream_button);
        if (pasturization_value == 0){delay(100); break;}
        if (butter_past_color_fade == 1){
        pixels_warm.fill(pixels_warm.Color(i,i,i));
        pixels_cold.fill(pixels_cold.Color(0,0,0));
        }
        else{
          pixels_warm.fill(pixels_warm.Color(255-i,255-i,255-i));
          pixels_cold.fill(pixels_cold.Color(0,0,0));
          pixels_butter.fill(pixels_butter.Color(0,0,0));
          pixels_homogenizer.fill(pixels_homogenizer.Color(0,0,0));
          
        }
        pixels_warm.show();
        pixels_cold.show();
        pixels_butter.show();
        pixels_homogenizer.show();
        
        delay(4);
        if (i == 255){butter_past_color_fade = butter_past_color_fade * -1;}
      }
      if (pasturization_value == 0){butter_past = 1; partGoal(1);}
      
      if (butter_past == 1){
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_warm.fill(pixels_warm.Color(i,0,0));
          pixels_warm.show();
          delay(4);
        }
        delay(200);
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_cold.fill(pixels_cold.Color(0,0,i));
          pixels_cold.show();
          delay(4);
        }
        butter_past_no_fade = 1;
        }}}
        if (butt_no_fade == 0){
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          butter_value = digitalRead(butter_button);
          if (butter_value == 0){delay(100); break;}
          if (butter_color_fade == 1){
          pixels_butter.fill(pixels_butter.Color(i,i,i));
          
          }
          else{
            pixels_butter.fill(pixels_butter.Color(255-i,255-i,255-i));
          }
          pixels_butter.show();
          delay(4);
          if (i > 254){butter_color_fade = butter_color_fade * -1;}
      }
        }
  if (butter_value == 0){butt = 1;}
  if (butt == 1 && pasturization_value == 0){
    butt_no_fade = 1;
    if(butter_finished == 0){
      pixels_butter.fill(pixels_butter.Color(255,125,0)); pixels_butter.show();
      partGoal(2);
      delay(1000);
    }
    }
  if (pasturization_value == 0 && butter_value == 0){butter_finished = 1; butter_past = 0; butter_past_no_fade=0; butter_past_color_fade=1; butt=0;}

}


void cheese(){
  butter_past = 0;
  butter_past_no_fade = 0;
  butter_past_color_fade = 1;
  hom = 0;
  hom_no_fade = 0;
  hom_color_fade = 1;
  int pasturization_value = digitalRead(skimmed_milk_button);
  int bacteria_value = digitalRead(bacteria_button);
  pinMode(cream_button, OUTPUT);
  digitalWrite(cream_button, LOW);

  if (past == 0){
    if (past_no_fade == 0){
      for(int i = 0; i < 256; i++){
        res = digitalRead(reset_button);
        digitalWrite(led_pin, HIGH);
        if (past_color_fade == 1){
        
        pixels_warm.fill(pixels_warm.Color(i,i,i));
        pixels_cold.fill(pixels_cold.Color(0,0,0));
        pixels_butter.fill(pixels_butter.Color(0,0,0));
        pixels_homogenizer.fill(pixels_homogenizer.Color(0,0,0));
        }
        else{
          pixels_warm.fill(pixels_warm.Color(255-i,255-i,255-i));
        }
        pixels_warm.show();
        pixels_cold.show();
        pixels_butter.show();
        pixels_homogenizer.show();
        delay(4);
        if (i > 254){past_color_fade = past_color_fade * -1;}
      }
      if (pasturization_value == 0){past = 1; partGoal(1);}
      
      if (past == 1){
        
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_warm.fill(pixels_warm.Color(i,0,0));
          pixels_warm.show();
          delay(4);
        }
        delay(200);
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_cold.fill(pixels_cold.Color(0,0,i));
          pixels_cold.show();
          delay(4);
        }
        past_no_fade = 1;
        }}}
  if (pasturization_value == 0 && bacteria_value == 0){partGoal(2); delay(1000); cheese_finished = 1; pinMode(cream_button, INPUT_PULLUP);}
  

}
void butter_milk_old(){
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  int bacteria_value = digitalRead(bacteria_button);

  if (homogenizer_value == 1){
    pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255)); pixels_homogenizer.show();
    }
  if (pasturization_value == 0){
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    }
  if (homogenizer_value == 1 && pasturization_value == 0 && bacteria_value == 0){butter_milk_finished = 1;}
}

void butter_milk(){
  milk_partgoal = 0;
  mil=0;
  mil_no_fade=0;
  mil_color_fade=1;
  past = 0;
  past_no_fade=0;
  past_color_fade=1;
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  int bacteria_value = digitalRead(bacteria_button);
  pinMode(cream_button, INPUT_PULLUP);

  
  if (hom == 0){
    if (hom_no_fade == 0){
      for(int i = 0; i < 126; i++){
        digitalWrite(led_pin, HIGH);
        res = digitalRead(reset_button);
        pasturization_value = digitalRead(skimmed_milk_button);
        if (butter_milk_partgoal == 0){
          if (hom_color_fade == 1){
          pixels_homogenizer.fill(pixels_homogenizer.Color(i,i,i));
          pixels_warm.fill(pixels_warm.Color(0,0,0));
          pixels_cold.fill(pixels_cold.Color(0,0,0));
          pixels_butter.fill(pixels_butter.Color(0,0,0));
          }
          else{
            pixels_homogenizer.fill(pixels_homogenizer.Color(125-i,125-i,125-i));
          }
          pixels_homogenizer.show();
          pixels_warm.show();
          pixels_cold.show();
          pixels_butter.show();
          delay(6);
          if (i == 125){hom_color_fade = hom_color_fade * -1;}
        }
        else{
          homogenizerBlink();
        }
        homogenizer_value = digitalRead(homogenizer_button);
        pasturization_value = digitalRead(skimmed_milk_button);
        
        if (pasturization_value == 0 && butter_milk_partgoal == 0){
          partGoal(1);
          butter_milk_partgoal++;
          pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255));
          pixels_homogenizer.show();
          }
        if (homogenizer_value == 1 && pasturization_value == 0){partGoal(2); hom = 1; delay(100); break;}
      }
      
      if (hom == 1){
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_homogenizer.fill(pixels_homogenizer.Color(i,i,i));
          pixels_homogenizer.show();
          delay(4);
        }
        
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_warm.fill(pixels_warm.Color(i,0,0));
          pixels_warm.show();
          delay(4);
        }
        delay(200);
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_cold.fill(pixels_cold.Color(0,0,i));
          pixels_cold.show();
          delay(4);
        }
        
        hom_no_fade = 1;
        }}}
  if (bacteria_value == 0 && hom == 1){
    butter_milk_finished = 1;
    butter_milk_partgoal = 0;
    hom=0;
    hom_no_fade=0;
    hom_color_fade=1;
    partGoal(3);
    digitalWrite(led_pin, LOW);
    delay(1000);
  }

}

void milk_old(){
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  if (homogenizer_value == 1){
    Serial.println("hom triggered");
    pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255)); pixels_homogenizer.show();}
  if (pasturization_value == 0){
    Serial.println("past triggered");
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    }
  if (homogenizer_value == 1 && pasturization_value == 0){milk_finished = 1;} 
}

void milk(){
  hom = 0;
  hom_no_fade = 0;
  hom_color_fade = 1;
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  pinMode(cream_button, INPUT_PULLUP);
  
  if (mil == 0){
    if (mil_no_fade == 0){
      for(int i = 0; i < 126; i++){
        digitalWrite(led_pin, HIGH);
        res = digitalRead(reset_button);
        pasturization_value = digitalRead(skimmed_milk_button);
        if (milk_partgoal == 0){
          if (mil_color_fade == 1){
          pixels_homogenizer.fill(pixels_homogenizer.Color(i,i,i));
          pixels_warm.fill(pixels_warm.Color(0,0,0));
          pixels_cold.fill(pixels_cold.Color(0,0,0));
          pixels_butter.fill(pixels_butter.Color(0,0,0));
          }
          else{
            pixels_homogenizer.fill(pixels_homogenizer.Color(125-i,125-i,125-i));
          }
          pixels_homogenizer.show();
          pixels_warm.show();
          pixels_cold.show();
          pixels_butter.show();
          delay(6);
          if (i == 125){mil_color_fade = mil_color_fade * -1;}
        }
        else{
          homogenizerBlink();
        }
        homogenizer_value = digitalRead(homogenizer_button);
        pasturization_value = digitalRead(skimmed_milk_button);
        if (pasturization_value == 0 && milk_partgoal == 0){
          partGoal(1);
          milk_partgoal++;
          pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255));
          pixels_homogenizer.show();
          }
        if (homogenizer_value == 1 && pasturization_value == 0){partGoal(2); mil = 1; delay(100); break;}
      }
      
      if (mil == 1){
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_homogenizer.fill(pixels_homogenizer.Color(i,i,i));
          pixels_homogenizer.show();
          delay(4);
        }
        
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_warm.fill(pixels_warm.Color(i,0,0));
          pixels_warm.show();
          delay(4);
        }
        delay(200);
        for(int i = 0; i < 256; i++){
          digitalWrite(led_pin, HIGH);
          pixels_cold.fill(pixels_cold.Color(0,0,i));
          pixels_cold.show();
          delay(4);
        }
        
        hom_no_fade = 1;
        }}}
  if (homogenizer_value == 1 && mil == 1){
    milk_finished = 1;
    milk_partgoal = 0;
    mil=0;
    mil_no_fade=0;
    mil_color_fade=1;
    digitalWrite(led_pin, LOW);
    delay(1000);
  }
}

void reset(){
  reset_leds();
  butter_finished = 0;
  butter_milk_finished = 0;
  milk_finished = 0;
  cheese_finished = 0;
  
  butter_credits_added = 0;
  butter_milk_credits_added = 0;
  milk_credits_added = 0;
  cheese_credits_added = 0;

  digitalWrite(four_way_led1, LOW);
  digitalWrite(four_way_led2, LOW);
  digitalWrite(four_way_led3, LOW);
  digitalWrite(four_way_led4, LOW);
  digitalWrite(homogenizer_led, LOW);
  started = 0;
  ready_for_payout = 0;
  completed_puzzles = 0;
  past = 0;
  butt = 0;
  past_no_fade = 0;
  butt_no_fade = 0;
  past_color_fade = 1;
  butter_color_fade = 1;
  butter_past = 0;
  hom = 0;
  mil = 0;
  butter_past_color_fade = 1;
  hom_color_fade = 1;
  mil_color_fade = 1;
  butter_past_no_fade = 0;
  hom_no_fade = 0;
  mil_no_fade = 0;
  butter_milk_partgoal = 0;
  milk_partgoal = 0;
}

void waitingToPayout(){
  if (millis() - prev > 250){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 500){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void waitingToStart(){
  if (millis() - prev > 3000){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 4000){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void homogenizerBlink(){
  if (millis() - hom_prev > 750){digitalWrite(homogenizer_led, HIGH);}
  if (millis() - hom_prev > 1500){digitalWrite(homogenizer_led, LOW); hom_prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100); 
  }

void payout(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  if ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){waitingToPayout(); return;}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    Serial.print(buffer_read[i]);
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  Serial.println();
  Serial.print("Total on card : ");
  total = total + puzzle_value + 1;
  int temp = total;
  Serial.print("Total after puzzle added: ");
  Serial.println(total);
  delay(250);
  
  while (temp >= 10){total_count++; temp = temp/10;}
  
  for (byte i = 0; i < total_count + 1; i++){
    if (i == 0){cash[total_count + 1] = 99;}
    cash[total_count - i] = total%10;
    total = total/10;  
    
  }
  Serial.println();
  for (byte i = 0; i < 16; i++){Serial.print(cash[i]);}
  Serial.println();


  //Write to the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  delay(500);
  //**************************************
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  playTone();
  reset();
  Serial.println("Done Reading");
}

void win(){
        int NOTE_SUSTAIN = 30;
         for(uint8_t nLoop = 0;nLoop < 2;nLoop ++)
         {
           tone(buzzer,1000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1100);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1200);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1300);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1400);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1500);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1600);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1700);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1800);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1900);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2100);
           delay(NOTE_SUSTAIN);
         }
         noTone(buzzer);
  }
void partGoal(int count){
    for (int i = 0; i < count; i++){
      tone(buzzer, 900 + 100*i);
      delay(100);
      noTone(buzzer);
      delay(100);
      }
     noTone(buzzer);
  }
  
void reset_leds(){
  pixels_warm.fill(pixels_warm.Color(0,0,0));
  pixels_cold.fill(pixels_cold.Color(0,0,0));
  pixels_butter.fill(pixels_butter.Color(0,0,0));
  pixels_homogenizer.fill(pixels_homogenizer.Color(0,0,0));
  pixels_warm.show();
  pixels_cold.show();
  pixels_butter.show();
  pixels_homogenizer.show();
  }
