#include <Adafruit_NeoPixel.h>

//******************************
#define skimmed_milk_button 4
#define cream_button 5
#define homogenizer_button 31
#define bacteria_button 7
#define butter_button 22
#define reset_button 2

#define pasturizing_cold_neopixel 10 //20 leds
#define pasturizing_warm_neopixel 6 //20 leds
#define homogenizer_neopixel 12 //4 leds
#define butter_neopixel 8 //4 leds

#define homogenizer_led 13
#define four_way_led1 34
#define four_way_led2 35
#define four_way_led3 36
#define four_way_led4 37

#define four_way_button1 24
#define four_way_button2 26
#define four_way_button3 28

#define led_pin 3



//*******************************

Adafruit_NeoPixel pixels_cold(20, pasturizing_cold_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_warm(20, pasturizing_warm_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_homogenizer(4, homogenizer_neopixel, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels_butter(4, butter_neopixel, NEO_GRB + NEO_KHZ800);

const int buzzer = 9;
const int milk_value = 25;
const int cheese_value = 75;
const int butter_milk_value = 55;
const int butter_value = 60;

int puzzle_value = 0;

int butter_finished = 0;
int butter_milk_finished = 0;
int milk_finished = 0;
int cheese_finished = 0;

int butter_credits_added = 0;
int butter_milk_credits_added = 0;
int milk_credits_added = 0;
int cheese_credits_added = 0;

unsigned long prev = millis();
unsigned long payout_timeout = 0;
unsigned long win_timer = 0;
int started = 0;
int ready_for_payout = 0;
String incomingValue = "";

void setup() {
  Serial.begin(9600);
  
  pinMode(skimmed_milk_button, INPUT_PULLUP);
  pinMode(cream_button, INPUT_PULLUP);
  pinMode(homogenizer_button, INPUT_PULLUP);
  pinMode(bacteria_button, INPUT_PULLUP);
  pinMode(butter_button, INPUT_PULLUP);
  pinMode(reset_button, INPUT_PULLUP);

  pinMode(four_way_button1, INPUT_PULLUP);
  pinMode(four_way_button2, INPUT_PULLUP);
  pinMode(four_way_button3, INPUT_PULLUP);

  pinMode(four_way_led1, OUTPUT);
  pinMode(four_way_led2, OUTPUT);
  pinMode(four_way_led3, OUTPUT);
  pinMode(four_way_led4, OUTPUT);
  pinMode(led_pin, OUTPUT);

  pixels_cold.begin();
  pixels_warm.begin();
  pixels_homogenizer.begin();
  pixels_butter.begin();

  pixels_cold.show();
  pixels_warm.show();
  pixels_homogenizer.show();
  pixels_butter.show();
  
  pixels_cold.setBrightness(50);
  pixels_warm.setBrightness(50);
  pixels_homogenizer.setBrightness(100);
  pixels_butter.setBrightness(100);
  reset();
}

void loop() {
  int x = digitalRead(four_way_button1);
  int y = digitalRead(four_way_button2);
  int z = digitalRead(four_way_button3);
  int res = digitalRead(reset_button);
  
  if (res == 0){started = 1; delay(500);}
  if (started == 0){waitingToStart(); return;}
  if (ready_for_payout == 0){digitalWrite(led_pin, HIGH);}
  res = digitalRead(reset_button);
  
  

  if (x == 1 && y == 0){butter();}
  else if (x == 1 && y == 1){milk();}
  else if (x == 0 && z == 0){butter_milk();}
  else if (x == 0 && z == 1){cheese();}
  
  if (butter_milk_finished == 1){digitalWrite(four_way_led2, HIGH); if (butter_milk_credits_added == 0){puzzle_value = puzzle_value + butter_milk_value;} butter_milk_credits_added = 1; win();}
  if (milk_finished == 1){digitalWrite(four_way_led1, HIGH); if (milk_credits_added == 0){puzzle_value = puzzle_value + milk_value;} milk_credits_added = 1; win();}
  if (butter_finished == 1){digitalWrite(four_way_led4, HIGH); if (butter_credits_added == 0){puzzle_value = puzzle_value + butter_value;} butter_credits_added = 1; win();}
  if (cheese_finished == 1){digitalWrite(four_way_led3, HIGH); if (cheese_credits_added == 0){puzzle_value = puzzle_value + cheese_value;} cheese_credits_added = 1; win();}
  if (butter_milk_finished == 1 || butter_finished == 1 || milk_finished == 1 || cheese_finished == 1){ready_for_payout = 1; if (millis() - payout_timeout > 1000){payout(); payout_timeout = millis();}}
  if (res == 0){reset();}
  
}


void butter(){
  int pasturization_value = digitalRead(cream_button);
  int butter_value = digitalRead(butter_button);
  int butter_finished = 0;
  if (butter_value == 0){butter_finished = 1;}

  if (pasturization_value == 0){
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    pixels_butter.fill(pixels_butter.Color(255,255,255)); pixels_butter.show();
    }
  if (butter_finished == 1){
    pixels_butter.fill(pixels_butter.Color(255,125,0)); pixels_butter.show();
    }
  if (pasturization_value == 0 && butter_value == 0){butter_finished = 1;}

}
void cheese(){
  int pasturization_value = digitalRead(skimmed_milk_button);
  int bacteria_value = digitalRead(bacteria_button);
  pinMode(cream_button, OUTPUT);
  digitalWrite(cream_button, LOW);

  if (pasturization_value == 0){
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    }
  if (pasturization_value == 0 && bacteria_value == 0){cheese_finished = 1; pinMode(cream_button, INPUT_PULLUP);}
  

}
void butter_milk(){
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  int bacteria_value = digitalRead(bacteria_button);

  if (homogenizer_value == 1){
    pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255)); pixels_homogenizer.show();
    }
  if (pasturization_value == 0){
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    }
  if (homogenizer_value == 1 && pasturization_value == 0 && bacteria_value == 0){butter_milk_finished = 1;}
}
void milk(){
  int homogenizer_value = digitalRead(homogenizer_button);
  int pasturization_value = digitalRead(skimmed_milk_button);
  if (homogenizer_value == 1){
    //Serial.println("hom triggered");
    /*pixels_homogenizer.fill(pixels_homogenizer.Color(255,255,255)); pixels_homogenizer.show();*/}
  if (pasturization_value == 0){
    //Serial.println("past triggered");
    /*
    pixels_warm.fill(pixels_warm.Color(255,0,0)); pixels_warm.show();
    pixels_cold.fill(pixels_cold.Color(0,0,255)); pixels_cold.show();
    */
    }
  if (homogenizer_value == 1 && pasturization_value == 0){milk_finished = 1;}
  
}

void reset(){
  Serial.println("Reset#");
  pixels_cold.fill(pixels_cold.Color(0,0,0));
  pixels_warm.fill(pixels_warm.Color(0,0,0));
  pixels_homogenizer.fill(pixels_homogenizer.Color(0,0,0));
  pixels_butter.fill(pixels_butter.Color(0,0,0));

  pixels_cold.show();
  pixels_warm.show();
  pixels_homogenizer.show();
  pixels_butter.show();

  butter_finished = 0;
  butter_milk_finished = 0;
  milk_finished = 0;
  cheese_finished = 0;
  
  butter_credits_added = 0;
  butter_milk_credits_added = 0;
  milk_credits_added = 0;
  cheese_credits_added = 0;
  incomingValue = "";

  digitalWrite(four_way_led1, LOW);
  digitalWrite(four_way_led2, LOW);
  digitalWrite(four_way_led3, LOW);
  digitalWrite(four_way_led4, LOW);
  started = 0;
  ready_for_payout = 1;
  delay(500);

}

void waitingToPayout(){
  if (millis() - prev > 250){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 500){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void waitingToStart(){
  if (millis() - prev > 3000){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 4000){digitalWrite(led_pin, LOW); prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100); 
  }

void win(){
        int NOTE_SUSTAIN = 30;
         for(uint8_t nLoop = 0;nLoop < 2;nLoop ++)
         {
           tone(buzzer,1000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1100);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1200);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1300);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1400);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1500);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1600);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1700);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1800);
           delay(NOTE_SUSTAIN);
           tone(buzzer,1900);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2000);
           delay(NOTE_SUSTAIN);
           tone(buzzer,2100);
           delay(NOTE_SUSTAIN);
         }
         noTone(buzzer);
  }

void payout(){
  Serial.print("Payout#");
  delay(100);
  if (Serial.available() > 0) {
    incomingValue = Serial.readStringUntil("#");
    if (incomingValue == "Value?#"){
      Serial.print(puzzle_value);
      while (!Serial.available() > 0) {}
        incomingValue = Serial.readStringUntil("#");
        int lastIndex = incomingValue.length() - 1;
        incomingValue.remove(lastIndex);
        puzzle_value = incomingValue.toInt();
    }
}
}
