//These can be changed
//**************************************************

float waitTime = 2; //In minutes. It will be approximately this time
int ledBrightness = 255;  // Set BRIGHTNESS to max (max = 255, min = 0)

//**************************************************

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define LED_COUNT 144
#define gasRelay 4
#define lighterRelay 5  
#define LED_PIN 6 //This pin is the data-line for the ledstrip
#define button 7  //Connect a button between this pin and ground
int buttonState;

int delayPixel = (waitTime * 60000) / (48 * 255 * 1.65);

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
// Argument 1 = Number of pixels in NeoPixel strip
// Argument 2 = Arduino pin number (most are valid)
// Argument 3 = Pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)

int rgb[] = {255, 125, 0};

void setup() {
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(ledBrightness); 
  pinMode(gasRelay, OUTPUT);
  pinMode(lighterRelay, OUTPUT);
  //pinMode(button, INPUT_PULLUP);
  pinMode(button, INPUT);
  Serial.begin(9600);
}

void loop() {
  countdown(); // White, half brightness
  delay(100);
}

void countdown() {
    for(int b=0; b<48; b++) { //  'b' counts from 0 to 2...
      int red = rgb[0];
      int green = rgb[1];
      int blue = rgb[2];
      int r = 0;
      int g = 0;

      int y = map(b, 0, 48, 0, green);

      while(g < y || r < red){
        if (r < red) r=r+1;
        if (g < y) g=g+1;
        //if (bl < blue) bl=bl+1;
        int z = map(r, 0, 255, 0, y); //Maps the value of green to change at the rate of red(green goes up about 1 by every 2 red)
        strip.setPixelColor(b, r, z, 0);
        strip.setPixelColor(95 - b, r, z, 0);
        strip.show();
        delay(delayPixel);
      }
    }
  strip.fill(strip.Color(0,255,0));
  strip.show();
  
  int reverse = 1;
  int bright = 125;
  
  while(true){
    Serial.println(bright);
    strip.fill(strip.Color(0, bright, 0));
    strip.show();
    bright = bright + reverse;
    delay(10);
    if (bright >= 255){reverse = -1;}
    else if(bright <= 1){reverse = 1;}
  
  buttonState = digitalRead(button);
  
    if (buttonState == HIGH){
      digitalWrite(lighterRelay, HIGH);   //Turn on the gas
      delay(500);                       
      digitalWrite(gasRelay, HIGH); //Turn on the lighter
      delay(1000); 
      digitalWrite(gasRelay, LOW);   //Turn off the gas 
      digitalWrite(lighterRelay, LOW);//Turn on the lighter
      delay(100);
      strip.clear();
      strip.show();
      return;
    }                      // wait for a second
  
  }
}
