#define led1 2
#define led2 3
#define led3 4
#define led4 5

#define b1 6
#define b2 7
#define b3 8
#define b4 9

#define button 12
#define led_pin 11

#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

const int buzzer = 10;
const int puzzle_value = 135;

unsigned long prev = millis();
int can_begin = 0;

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  // put your setup code here, to run once:
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);

  pinMode(b1, INPUT_PULLUP);
  pinMode(b2, INPUT_PULLUP);
  pinMode(b3, INPUT_PULLUP);
  pinMode(b4, INPUT_PULLUP);

  pinMode(button, INPUT_PULLUP);
  pinMode(led_pin, OUTPUT);
  pinMode(buzzer, OUTPUT);
  Serial.begin(9600);
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
}

void loop() {
  // put your main code here, to run repeatedly:
  int b1v = digitalRead(b1);
  int b2v = digitalRead(b2);
  int b3v = digitalRead(b3);
  int b4v = digitalRead(b4);
  int start_button = digitalRead(button);
  while (can_begin == 0){
    start_button = digitalRead(button);
    b1v = digitalRead(b1);
    b2v = digitalRead(b2);
    b3v = digitalRead(b3);
    b4v = digitalRead(b4);
    waitingToStart();
    //Serial.println(start_button);
    if (b1v == 1 && b2v == 1 && b3v == 1 && b4v == 1 && start_button == 0){can_begin = 1; delay(500);}
    }
  start_button = digitalRead(button);
  digitalWrite(led_pin, HIGH);

  digitalWrite(led1, !b1v);
  digitalWrite(led2, !b2v);
  digitalWrite(led3, !b3v);
  digitalWrite(led4, !b4v);

  if (b1v == 0 && b2v == 0 && b3v == 0 && b4v == 0){
    payout();
    reset();
    /* Payout function */
    }
  if (start_button == 0){reset();}
  
}

void reset(){
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
  digitalWrite(led4, LOW);
  can_begin = 0;
  }

void waitingToPayout(){
  if (millis() - prev > 250){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 500){digitalWrite(led_pin, LOW); prev = millis();}
  }
  
void waitingToStart(){
  if (millis() - prev > 3000){digitalWrite(led_pin, HIGH);}
  if (millis() - prev > 4000){digitalWrite(led_pin, LOW); prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100);
    
  }
  
void payout(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte clean[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  

  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  int total_list[] = {};
  
  while ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){int start_button = digitalRead(button); if (start_button == 0){can_begin = 0; delay(500); return;}waitingToPayout();}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  
  for (byte i = 0; i < 16; i++){
    total = total + buffer_read[i]*pow(10,i); 
    Serial.print(buffer_read[i]); 
    Serial.print(" : ");
    }
  Serial.print("Total is:");
  Serial.println(total);
  Serial.print("*******");
  Serial.println();
  total = total + puzzle_value;
  Serial.print(total);
  Serial.println();
  String str_total = String(total);
  int total_len = str_total.length();

  for (int i = 0; i < total_len; i++ ){
    if(i == 0){
      total_list[i] = total%10;
      Serial.println(total_list[i]);
    } 
    else{
      int temp = total/pow(10,i); 
      total_list[i] = temp%10;
      Serial.println(temp);
      Serial.println(" : ");
      Serial.println(total_list[i]);
    }
}
  
  for (byte i = 0; i < total_len; i++ ){
    cash[i] = total_list[total_len - (i + 1)];
    Serial.println(cash[byte(0)]);
    Serial.println(cash[byte(1)]);
    Serial.println(cash[byte(2)]);
    //Serial.print(cash[i]);
    //Serial.print(" : ");
    }
  
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  playTone();
  delay(1000);
  
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  Serial.println("Done Reading");
  
  for (byte i = 0; i < 16; i++){Serial.print(buffer_read[i]);/*total = total + buffer_read[i];*/}
  Serial.println();
  Serial.println(total);
  Serial.println("Done reading");
  
}
