

/*
 * --------------------------------------------------------------------------------------------------------------------
 * Example showing how to read data from a PICC to serial.
 * --------------------------------------------------------------------------------------------------------------------
 * This is a MFRC522 library example; for further details and other examples see: https://github.com/OSSLibraries/Arduino_MFRC522v2
 * 
 * Example sketch/program showing how to read data from a PICC (that is: a RFID Tag or Card) using a MFRC522 based RFID
 * Reader on the Arduino SPI interface.
 * 
 * When the Arduino and the MFRC522 module are connected (see the pin layout below), load this sketch into Arduino IDE
 * then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
 * you present a PICC (that is: a RFID Tag or Card) at reading distance of the MFRC522 Reader/PCD, the serial output
 * will show the ID/UID, type and any data blocks it can read. Note: you may see "Timeout in communication" messages
 * when removing the PICC from reading distance too early.
 * 
 * If your reader supports it, this sketch/program will read all the PICCs presented (that is: multiple tag reading).
 * So if you stack two or more PICCs on top of each other and present them to the reader, it will first output all
 * details of the first and then the next PICC. Note that this may take some time as all data blocks are dumped, so
 * keep the PICCs at reading distance until complete.
 * 
 * @license Released into the public domain.
 * 
 * Typical pin layout used:
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
 *
 * Not found? For more see: https://github.com/OSSLibraries/Arduino_MFRC522v2#pin-layout
 */

#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

const int buzzer = 9;
MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.
//mfrc522.PCD_SetAntennaGain(0x80);

void setup() {
  Serial.begin(9600);  // Initialize serial communications with the PC for debugging.
  pinMode(buzzer, OUTPUT);
  while (!Serial);     // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4).
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
	Serial.println(F("Ready to scan:"));
  
  
  


}

void loop() {
	// Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
	

  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 7;
  byte valueBlockA    = 4;
  byte block;
  byte len;
  byte buffer[16];
  byte clean[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  
  if (Serial.available() > 0){
  len = Serial.readBytesUntil('#', (char *) buffer, 16) ; // read family name from serial
  for (byte i = len; i < 16; i++)buffer[i] = 0;
  for (int i = 0; i < 16; i++){Serial.print(char(buffer[i]));}
  for (int i = 0; i < 16; i++){Serial.print(buffer[i]);}
  }
  else{
    return;
    }
  if ( !mfrc522.PICC_IsNewCardPresent()) return;
  if ( !mfrc522.PICC_ReadCardSerial())   return;
  
  
  //byte status;
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  //status = mfrc522.MIFARE_Write(valueBlockA, buffer, (byte)16);
  status = mfrc522.MIFARE_Write(valueBlockA, buffer, (byte)16);
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  Serial.println("Information written");
  playTone();

  byte x[18];
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, x, (byte)16);
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  for (int i = 0; i < 16; i++){Serial.print(char(x[i]));}
  for (int i = 0; i < 16; i++){Serial.print(x[i]);}
  //playTone();
}



void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(3000);
  }
