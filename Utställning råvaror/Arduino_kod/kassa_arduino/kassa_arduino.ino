#include <MFRC522Constants.h>
#include <MFRC522Driver.h>
#include <MFRC522Hack.h>
#include <MFRC522v2.h>
#include <require_cpp11.h>
#include <MFRC522v2.h>
#include <MFRC522DriverI2C.h>
#include <MFRC522DriverPinSimple.h>
#include <MFRC522Debug.h>

#define button 2
#define led 3
#define buzzer 9

int clean_card = 0;
byte clean_buffer[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
byte name_buffer[17];
byte len;
int reset_shop = 1;
unsigned long reset_timer = 0;
unsigned long prev = 0;
String incomingValue = "";
int capital_read = 0;
int payvalue = 0;
int funds = 0;
int totalToPay = 0;
int payed = 0;

MFRC522DriverI2C driver{0x28}; // Create I2C driver.
MFRC522 mfrc522{driver};  // Create MFRC522 instance.

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(button, INPUT_PULLUP);
  mfrc522.PCD_Init();  // Init MFRC522 board.
  mfrc522.PCD_SetAntennaGain(0x07<<4);
}

void loop() {
  waitingToStart();
  // put your main code here, to run repeatedly:
  reset_shop = digitalRead(button);
  if (reset_shop == 0){Serial.print("Reset#"); delay(1000); funds=0; totalToPay=0;}
  
  
  
  if (Serial.available() > 0) {
      payvalue = Serial.parseInt();
      totalToPay = totalToPay + payvalue;
      payvalue = 0;
      }
  if (funds == 0){
    funds = read_capital();
    if (funds > 0){
      Serial.print(funds);
      delay(1000);
    }
  }
  else if(totalToPay > 0 &&  funds > totalToPay){
    while (true){
      if (Serial.available() > 0) {
        String payval = Serial.readString();
        payvalue = payval.toInt();
        totalToPay = totalToPay + payvalue;
        payval = "";
        payvalue = 0;
      }
      waitingToPay();
      payed = pay(totalToPay);
      reset_shop = digitalRead(button);
      if (reset_shop == 0){
        Serial.print("Reset#");
        funds = 0;
        totalToPay = 0;
        delay(1000);
        }
       if (payed == 1){
        Serial.print("Payed#");
        funds = 0;
        totalToPay = 0;
        delay(1000);
        break;
        }
    }
    totalToPay = 0;
  }
  else if(funds < totalToPay){
    /*Serial.print("declined");*/
    }
}
void waitingToStart(){
  if (millis() - prev > 2000){digitalWrite(led, HIGH);}
  if (millis() - prev > 3000){digitalWrite(led, LOW); prev = millis();}
  }

void waitingToPay(){
  if (millis() - prev > 1000){digitalWrite(led, HIGH);}
  if (millis() - prev > 1500){digitalWrite(led, LOW); prev = millis();}
  }

void playTone(){
  tone(buzzer, 1000);
  delay(300);
  tone(buzzer, 2000);
  delay(100);
  noTone(buzzer);
  delay(100);
  }

int read_capital(){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  
  if(!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){return 0;}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  total = total + 1;
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  playTone();
  return total;
  }
  
int pay(int deduct){
  MFRC522::StatusCode status;
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  byte trailerBlock   = 11;
  byte valueBlockA    = 8;
  byte block;
  byte len;
  byte buffer_write[16];
  byte buffersize_write = sizeof(buffer_write);
  byte cash[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  byte buffer_read[18];
  byte buffersize_read = sizeof(buffer_read);
  int total = 0;
  byte total_list[] = {};
  int count = 0;
  int total_count = 0;
  
  if ( !mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()){return 0;}
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  //Read the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Read(valueBlockA, buffer_read, &buffersize_read);
  delay(500);
  //**************************************
  
  for (byte i = 0; i < 16; i++){
    if (buffer_read[i] == 99){break;}
    /*Serial.print(buffer_read[i]);*/
    count = count + 1;
  }
  for (byte i = 0; i < count; i++){
    int temp = buffer_read[i] * pow(10,count - (i + 1));
    total = total + temp;  
  }
  total = total;
  //Serial.println();
  //Serial.print("Total on card : ");
  //Serial.println(total);
  total = total - deduct;
  int temp = total;
  //Serial.print("Total after puzzle added: ");
  //Serial.println(total);
  
  while (temp >= 10){total_count++; temp = temp/10;}
  
  for (byte i = 0; i < total_count + 1; i++){
    if (i == 0){cash[total_count + 1] = 99;}
    cash[total_count - i] = total%10;
    total = total/10;  
    
  }
  //Serial.println();
  //for (byte i = 0; i < 16; i++){Serial.print(cash[i]);}
  //Serial.println();


  //Write to the card
  //**************************************
  status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
  status = mfrc522.MIFARE_Write(valueBlockA, cash, buffersize_write);
  delay(500);
  //**************************************
  
  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
  playTone();
  return 1;
  //Serial.println("Done Reading");
}
