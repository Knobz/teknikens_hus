# teknikens_hus

## How to write to rfid card

```C
MFRC522::StatusCode status;
MFRC522::MIFARE_Key key;
for (byte i = 0; i < 6; i++) {key.keyByte[i] = 0xFF;}
byte trailerBlock   = 7;
byte valueBlockA    = 4;
byte block;
byte len;
byte buffer[16];
  
if (Serial.available() > 0){
    len = Serial.readBytesUntil('#', (char *) buffer, 16) ;
    // Read the name from serial, max 16 letters
    for (byte i = len; i < 16; i++)buffer[i] = 0;
    //Add name to buffer, fill the rest with zeros
    for (int i = 0; i < 16; i++){Serial.print(char(buffer[i]));}
    for (int i = 0; i < 16; i++){Serial.print(buffer[i]);}
    // Print the name then the bytes
}
else{
return;
// Makes it wait for serial to be transmitted
}
if ( !mfrc522.PICC_IsNewCardPresent()) return;
// Wait for new card
if ( !mfrc522.PICC_ReadCardSerial())   return;
// Read uid and select card

status = mfrc522.PCD_Authenticate(MFRC522Constants::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key, &(mfrc522.uid));
// Authenticate the block with standard key
// Trailerblock is the last block in each sector
// 3, 7, 11, 15 and so on
status = mfrc522.MIFARE_Write(valueBlockA, buffer, (byte)16);
// Write buffer to the block
mfrc522.PICC_HaltA();
// Stop the card from doing anything more
mfrc522.PCD_StopCrypto1();
// Stop encryption so that it can be accessed again
´´´
