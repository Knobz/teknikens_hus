import cv2
#import RPi.GPIO as GPIO
#import time

cap = cv2.VideoCapture('train.mp4') #open video
frameTime = 16 # time of each frame in ms 16.7 is 60fps
SAMPLE = 0


# declare GPIO mode
#GPIO.setmode(GPIO.BCM)

# define GPIO pins with variables charge_pin and measure_pin
# 18=chargepin 23=measure
charge_pin = 18 
measure_pin = 23

# discharge the capacitor
def discharge():
    GPIO.setup(charge_pin, GPIO.IN) #stop charging
    GPIO.setup(measure_pin, GPIO.OUT) #make the measure pin an output
    GPIO.output(measure_pin, False) # set it low to discharge the capacitor
    time.sleep(0.005)

def charge_time():
    GPIO.setup(measure_pin, GPIO.IN) 
    GPIO.setup(charge_pin, GPIO.OUT)
    count = 0
    GPIO.output(charge_pin, True) #start charging the capacitor
    while not GPIO.input(measure_pin): #wait till the pin goes HIGH
        count = count +1
    return count

def analog_read():
    discharge()
    return charge_time()

def read_potentiometer():
    return 80

while(cap.isOpened()):
    SAMPLE += 1
    ret, frame = cap.read() #read the current frame from the video
    frame = cv2. resize(frame, (1920,1080)) #resize if needed
    cv2.imshow('frame',frame)   #show the video in a window
    """if SAMPLE == 10:
        input_value = analog_read()
        print(input_value)
        SAMPLE = 0"""
    frameTime = read_potentiometer()
    if cv2.waitKey(frameTime) & 0xFF == ord('q'):   #If you press q the window will be destroyed
        break
    
cap.release()
cv2.destroyAllWindows()